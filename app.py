import numpy as np
import torch
from flask import Flask, jsonify, redirect, request

from src.ml_ops_bloc_3_docker import SimpleNN

PATH_WEIGHTS = "weights.pth"

app = Flask(__name__)

model = SimpleNN()
model.eval()


@app.route("/")
def hello():
    return redirect("/predict", code=302)


@app.route("/predict", methods=["POST"])
def predict():
    if request.method == "POST":
        data = request.json
        x = np.array(data["x"], dtype=np.float64).reshape(-1, 1)

        # Преобразуем в тензор PyTorch
        input_tensor = torch.from_numpy(x)

        # Получаем прогноз от модели
        with torch.no_grad():
            prediction = model(input_tensor)

        # Преобразуем обратно в numpy array
        prediction = np.array(prediction)

        return jsonify({"prediction": prediction.tolist()})
    return jsonify({"prediction": None})


if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0")

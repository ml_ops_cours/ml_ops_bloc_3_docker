# ml_ops_bloc_3_docker

В этом блоке рассмотрен процесс создание docker image в рамках курса.

## 1. Установка

### Установка с помощью Docker image:

Создание Docker image.

```bash
docker build . -t ml_obs_bloc_3_docker
```

Разворачивание Docker image.

```bash
docker run  -p 5000:5000 ml_obs_bloc_3_docker
```

### Запуск локально:

Установите зависимости.

```bash
pdm install
```

Запуск flask app

```bash
pdm run app.py
```

## 2. Отправка запроса к модели

```bash
curl --header "Content-Type: application/json" \
  --request POST \
  --data '{"x":"0.25"}' \
  http://localhost:5000/predict
```

## 3. Обучение модели

Раздел в разработке.

## 4. Вклад

Мы приветствуем вклады от сообщества! Здесь описаны основные принципы, которые следует учитывать при внесении изменений. Подробнее в [CONTRIBUTING.md](/CONTRIBUTING.md).

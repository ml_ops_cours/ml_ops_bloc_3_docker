FROM python:3.9

WORKDIR /app

COPY . /app/

RUN pip install pdm

RUN pdm install

ENTRYPOINT ["pdm", "run", "app.py"]

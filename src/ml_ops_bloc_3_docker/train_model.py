import click
import numpy as np
import torch
import torch.nn as nn
import torch.optim as optim

from .model import SimpleNN


@click.command()
@click.option("--epochs", type=int, default=1000, help="Number of epochs for training")
@click.option("--lr", type=float, default=0.01, help="Learning rate")
def train(epochs: int, lr: float) -> None:
    """Train a simple neural network

    Args:
        epochs (int): _description_
        lr (float): _description_
    """
    x_train = np.random.rand(1000, 1).astype(np.float64)
    y_train = 2 * x_train + 1 + np.random.randn(1000, 1) * 0.1

    model = SimpleNN()

    criterion = nn.MSELoss()
    optimizer = optim.SGD(model.parameters(), lr=lr)

    for epoch in range(epochs):
        inputs = torch.from_numpy(x_train)
        labels = torch.from_numpy(y_train)

        optimizer.zero_grad()

        outputs = model(inputs)
        loss = criterion(outputs, labels)

        loss.backward()
        optimizer.step()

        if (epoch + 1) % 100 == 0:
            print(f"Epoch [{epoch+1}/{epochs}], Loss: {loss.item():.4f}")

    torch.save(model.state_dict(), "weights.pth")


if __name__ == "__main__":
    train()

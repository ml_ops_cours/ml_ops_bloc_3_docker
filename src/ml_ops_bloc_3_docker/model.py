import torch.nn as nn


class SimpleNN(nn.Module):
    def __init__(self):
        super(SimpleNN, self).__init__()
        self.fc = nn.Linear(1, 1)
        self.double()

    def forward(self, x) -> None:
        return self.fc(x)

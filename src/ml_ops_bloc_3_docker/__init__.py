# flake8: noqa

from .model import SimpleNN
from .train_model import train
